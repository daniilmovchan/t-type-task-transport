1. Install python
2. Open CMD and follow next commands:
```
python -m venv venv
.\venv\Scripts\activate
python install -r requirements.txt
cd ttype
python manage.py runserver
```
[main page](./img/1.PNG)
[result page](./img/2.PNG)