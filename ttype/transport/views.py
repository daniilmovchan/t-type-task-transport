from django.http import HttpResponse
from django.shortcuts import render
from transport.services.ttype_transport import calculate_ttype_transport

def index(request):
    return render(request, 'transport/index.html')


def calculate(request):
    if request.method == 'POST':
        data = request.POST
        result_x, cost = calculate_ttype_transport(data)
        if len(result_x) == 9:
            return render(request, 'transport/result.html', {'cost': cost,
                                                             'x1': result_x[0], 'x2': result_x[1], 'x3': result_x[2],
                                                             'x4': result_x[3], 'x5': result_x[4], 'x6': result_x[5],
                                                             'x7': result_x[6], 'x8': result_x[7], 'x9': result_x[8],
                                                             'a1': data['a1'], 'a2': data['a2'], 'a3': data['a3'],
                                                             'b1': data['b1'], 'b2': data['b2'], 'b3': data['b3'],
                                                             })
        return HttpResponse("Error!")  # methods must return HttpResponse