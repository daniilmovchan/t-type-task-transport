from django.urls import path

from transport.views import *

urlpatterns = [
    path('', index),
    path('calculate/', calculate),
]