import time

from pulp import LpVariable, LpProblem, LpMaximize, value


def calculate_ttype_transport(data):
    c = get_data(data, 'c', 9)
    a = get_data(data, 'a', 3)
    b = get_data(data, 'b', 3)
    x1 = LpVariable("x1", lowBound=0)
    x2 = LpVariable("x2", lowBound=0)
    x3 = LpVariable("x3", lowBound=0)
    x4 = LpVariable("x4", lowBound=0)
    x5 = LpVariable("x5", lowBound=0)
    x6 = LpVariable("x6", lowBound=0)
    x7 = LpVariable("x7", lowBound=0)
    x8 = LpVariable("x8", lowBound=0)
    x9 = LpVariable("x9", lowBound=0)

    problem = LpProblem('0', LpMaximize)
    problem += -c[0] * x1 - c[1] * x2 - c[2] * x3 - c[3] * x4 - c[4] * x5 - c[5] * x6 - c[6] * x7 - c[7] * x8 - c[8] * x9, "Функция цели"

    problem += x1 + x2 + x3 <= a[0], "1"
    problem += x4 + x5 + x6 <= a[1], "2"
    problem += x7 + x8 + x9 <= a[2], "3"
    problem += x1 + x4 + x7 == b[0], "4"
    problem += x2 + x5 + x8 == b[1], "5"
    problem += x3 + x6 + x9 == b[2], "6"
    problem.solve()
    x_solve = []
    for variable in problem.variables():
        x_solve.append(variable.varValue)
    return x_solve, abs(value(problem.objective))


def get_data(data, letter, amount):
    arr = []
    for i in range(amount):
        arr.append(int(data[f'{letter}{i+1}']))
    return arr